// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.utils;

/** Add your docs here. */
public class MathUtil {

  public static double round(double number, int places) {
    return Math.round(number * Math.pow(10, places)) / Math.pow(10, places);
  }
}
