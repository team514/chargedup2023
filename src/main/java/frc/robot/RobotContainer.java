
// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.io.IOException;
import java.nio.file.Path;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RepeatCommand;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.commands.BalanceBot;
import frc.robot.commands.CenterToTarget;
import frc.robot.commands.OperateArm;
import frc.robot.commands.OperateDrive;
import frc.robot.commands.OperateExtension;
import frc.robot.commands.OperateGripper;
import frc.robot.commands.SetLEDColor;
import frc.robot.commands.autoblocks.VisionDriveToDistance;
import frc.robot.commands.autosequences.AutoPlaceBalance;
import frc.robot.commands.autosequences.AutoPlaceDrive;
import frc.robot.commands.autosequences.AutoPlacePiece;
import frc.robot.commands.autosequences.AutoTaxi;
import frc.robot.commands.autosequences.MoveArmToGoal;
import frc.robot.handlers.LEDColor;
import frc.robot.handlers.MatchState;
import frc.robot.subsystems.ArmUtil;
import frc.robot.subsystems.CrabUtil;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.ExtensionUtil;
import frc.robot.subsystems.LEDUtil;
import frc.robot.subsystems.VisionUtil;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private static final DriveUtil driveUtil = new DriveUtil();
  private static final VisionUtil visionUtil = new VisionUtil();
  private static final ArmUtil armUtil = new ArmUtil();
  private static final CrabUtil crabUtil = new CrabUtil();
  private static final ExtensionUtil extensionUtil = new ExtensionUtil();
  private static final LEDUtil ledUtil = new LEDUtil();

  // Joysticks declared
  private static Joystick leftJoystick, rightJoystick;

  // Buttons declared 
  private static JoystickButton balance, centerToTarget, driveToDistance,
      resetEncoders, crabToggle, moveArmSubstation, moveArmMid, moveArmHigh, moveArmToSafety,
      purpleLEDs, yellowLEDs;

  // Operator controller declared    
  private static XboxController operator;

  // Robot is in the PREMATCH MatchState and position is unknown
  private static MatchState matchState = MatchState.PREMATCH;
  private static Pose2d currentPosition = null;

  // Auto
  private static Trajectory trajectory;
  private SendableChooser<Command> autoChooser;

  // SHUFFLEBOARD
  ShuffleboardTab autoTab;

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    leftJoystick = new Joystick(Constants.leftJoystick);
    rightJoystick = new Joystick(Constants.rightJoystick);
    operator = new XboxController(Constants.kOperatorControllerPort);
    // Configure the trigger bindings
    configureBindings();
    // Other initial configurations
    initDefaultCommands();
    setupAutoSendable();

    // Let driveteam choose the auto mode
    SmartDashboard.putData("Autonomous Path", autoChooser);
  }

  /**
   * Use this method to define your trigger->command mappings. Triggers can be created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with an arbitrary
   * predicate, or via the named factories in {@link
   * edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses for {@link
   * CommandXboxController Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller
   * PS4} controllers or {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick Flight
   * joysticks}.
   */
  private void configureBindings() {
    // Put the commands on the correct buttons

    // Balance on the Charged Station
    balance = new JoystickButton(rightJoystick, Constants.balanceButton);
    balance.whileTrue(new BalanceBot(driveUtil));

    // Strafe until directly in front of an AprilTag
    centerToTarget = new JoystickButton(leftJoystick, 1);
    centerToTarget.whileTrue(new RepeatCommand(new CenterToTarget(driveUtil, visionUtil)));

    // Drive up to vision target
    driveToDistance = new JoystickButton(leftJoystick, 2);
    driveToDistance.whileTrue(new RepeatCommand(new VisionDriveToDistance(driveUtil, visionUtil)));

    // Reset encoders manually
    resetEncoders = new JoystickButton(rightJoystick, 6);
    resetEncoders.onTrue(new InstantCommand(() -> driveUtil.resetEncoders(), driveUtil));

    // // Move arm up
    // moveArmUp = new JoystickButton(operator, Button.kRightBumper.value);
    // moveArmUp.whileTrue(new MoveArm(armUtil, true));

    // // Move arm down
    // moveArmDown = new JoystickButton(operator, Button.kLeftBumper.value);
    // moveArmDown.whileTrue(new MoveArm(armUtil, false));

    // Move arm to setpoint (PID)
    moveArmSubstation = new JoystickButton(operator, Button.kA.value);
    // moveArmLow.onTrue(new MoveArmPID(armUtil, Constants.lowSetpoint));
    moveArmSubstation.whileTrue(new MoveArmToGoal(Constants.substationSetpoint, Constants.substationExtension, armUtil, extensionUtil));

    moveArmMid = new JoystickButton(operator, Button.kB.value);
    // moveArmMid.onTrue(new MoveArmPID(armUtil, Constants.mediumSetpoint));
    moveArmMid.whileTrue(new MoveArmToGoal(Constants.mediumSetpoint, Constants.mediumExtension, armUtil, extensionUtil));

    moveArmHigh = new JoystickButton(operator, Button.kY.value);
    // moveArmHigh.onTrue(new MoveArmPID(armUtil, Constants.highSetpoint));
    moveArmHigh.whileTrue(new MoveArmToGoal(Constants.reverseHighSetpoint, Constants.reverseHighExtension, armUtil, extensionUtil));

    moveArmToSafety = new JoystickButton(operator, Button.kX.value);
    moveArmToSafety.whileTrue(new MoveArmToGoal(Constants.safetySetpoint, Constants.safetyExtension, armUtil, extensionUtil));

    // Toggle claw
    crabToggle = new JoystickButton(operator, Button.kRightBumper.value);
    crabToggle.onTrue(new OperateGripper(crabUtil));

    // extendArmOut = new JoystickButton(operator, Button.kStart.value);
    // extendArmOut.whileTrue(new ExtendArm(extensionUtil, true));

    // retractArmHome = new JoystickButton(operator, Button.kX.value);
    // retractArmHome.onTrue(new ExtendArmEncoder(Constants.armExtensionMinTicks, extensionUtil));

    purpleLEDs = new JoystickButton(leftJoystick, 1);
    purpleLEDs.onTrue(new SetLEDColor(ledUtil, LEDColor.PURPLE));
    yellowLEDs = new JoystickButton(rightJoystick, 1);
    yellowLEDs.onTrue(new SetLEDColor(ledUtil, LEDColor.YELLOW));
  }

  private void initDefaultCommands() {
    driveUtil.setDefaultCommand(new OperateDrive(driveUtil));
    armUtil.setDefaultCommand(new OperateArm(armUtil));
    extensionUtil.setDefaultCommand(new OperateExtension(extensionUtil));
    //extensionUtil.setDefaultCommand(new OperateExtension(extensionUtil));
  }

  // Get Joystick Values

  public static double getDriverLeftJoystickX() {
    return leftJoystick.getX();
  }

  public static double getDriverLeftJoystickY() {
    return leftJoystick.getY();
  }

  public static double getDriverLeftJoystickZ() {
    return leftJoystick.getZ();
  }

  public static double getDriverRightJoystickX() {
    return rightJoystick.getX();
  }

  public static double getDriverRightJoystickY() {
    return rightJoystick.getY();
  }

  public static double getDriverRightJoystickZ() {
    return rightJoystick.getZ();
  }

  public static double getOperatorLeftStick() {
    return -operator.getLeftY();
  }

  public static double getRightStick() {
    return -operator.getRightY();
  }

  // MatchState

  public static void setMatchState(MatchState newMatchState) {
    matchState = newMatchState;
  }

  public static MatchState getMatchState() {
    return matchState;
  }

  // Trajectory

  public static void setCurrentPosition(Pose2d pose2d) {
    currentPosition = pose2d;
  }

  public static Pose2d getCurrentPosition() {
    return currentPosition;
  }

  public static Trajectory getTrajectory() {
    return trajectory;
  }

  private void setupAutoSendable() {
    // Dropdown chooser on Shuffleboard with enum PathChoice options for auto trajectory
    autoChooser = new SendableChooser<Command>();
    autoChooser.setDefaultOption("Place One Piece", new AutoPlacePiece(driveUtil, crabUtil, armUtil, extensionUtil));
    autoChooser.addOption("Place and Drive", new AutoPlaceDrive(driveUtil, crabUtil, armUtil, extensionUtil));
    autoChooser.addOption("Taxi", new AutoTaxi(driveUtil, armUtil, extensionUtil));
    autoChooser.addOption("Place, Drive, and Balance",
        new AutoPlaceBalance(driveUtil, armUtil, extensionUtil, crabUtil));
  }

  public static void setTrajectory(String trajectoryJSON) {
    // Sets trajectory 
    try {
      // trajectoryPath = the path to the trajectory; trajectory = trajectory found at trajectoryPath
      Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(trajectoryJSON);
      trajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
    } catch (IOException ex) {
      // Throws error if it can't open the trajectory file
      DriverStation.reportError("Unable to open trajectory: " + trajectoryJSON, ex.getStackTrace());
    }
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An example command will be run in autonomous
    // return new DriveTestSequence(driveUtil);
    // return new TrajectoryTest(driveUtil);
    return autoChooser.getSelected();
  }

  public static DriveUtil getDriveUtil() {
    return driveUtil;
  }

  public static VisionUtil getVisionUtil() {
    return visionUtil;
  }
}
