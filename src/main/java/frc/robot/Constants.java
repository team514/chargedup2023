// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation3d;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

    // Joystick/Controller IDs
    public static final int leftJoystick = 0;
    public static final int rightJoystick = 1;
    public static final int kOperatorControllerPort = 2;

    // Operator Button Mappings
    public static final int balanceButton = 4;

    /* Trajectory Paths */
    public static final String testTrajectoryPath = "paths/Test Path.wpilib.json";

    /* DriveUtil */

    // Drive Base CAN IDs
    public static final int frontLeft = 3;
    public static final int frontRight = 4;
    public static final int backLeft = 1;
    public static final int backRight = 2;

    // Encoders
    public static final int backRightEncoder_channelA = 1;
    public static final int backRightEncoder_channelB = 0;
    public static final int backLeftEncoder_channelA = 3;
    public static final int backLeftEncoder_channelB = 2;
    public static final int frontRightEncoder_channelA = 5;
    public static final int frontRightEncoder_channelB = 4;
    public static final int frontLeftEncoder_channelA = 6;
    public static final int frontLeftEncoder_channelB = 7;

    // Encoder Constants
    public static final double ticksPerFoot = 220.28;
    public static final double ticksPerMeter = 722.70;
    public static final double ticksPerFootLateral = 200;

    /* ArmUtil */
    public static final int armExtensionMotorController = 5;
    public static final int leftArmMotor = 6;
    public static final int rightArmMotor = 7;
    public static final int extensionEncoder_channelA = 9;
    public static final int extensionEncoder_channelB = 8;

    public static final int forwardLimiter = 0;
    public static final int backwardLimiter = 1;

    public static final int armExtensionMaxTicks = 290;
    public static final int armExtensionMinTicks = -30;

    public static final double lowerDesertTicks = 15.28;
    public static final double upperDesertTicks = 43.19;

    public static final double substationSetpoint = 15.333;
    public static final double mediumSetpoint = 11;
    public static final double highSetpoint = 20;
    public static final double safetySetpoint = 25;

    public static final double reverseHighSetpoint = 43;

    /* ExtensionUtil */

    public static final double substationExtension = 100;
    public static final double mediumExtension = 200;
    public static final double highExtension = 300;
    public static final double safetyExtension = 0;

    public static final double reverseHighExtension = 275;


    /* VisionUtil */

    // Camera Constants
    public static final Transform3d robotToCamera = new Transform3d(new Translation3d(0.5, 0.0, 0.5), new Rotation3d(0, 0, 0));

    /* LEDUtil */
    public static final int ledStripPWM = 0;
}
