// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoblocks;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.DriveUtil;

public class AutoDriveDistance extends CommandBase {
  DriveUtil driveUtil;
  double velocity;
  double distanceInMeters;
  boolean done = false;

  /** Creates a new AutoDriveDistance. */
  public AutoDriveDistance(DriveUtil driveUtil, double velocity, double distanceInMeters) {
    this.driveUtil = driveUtil;
    this.velocity = velocity;
    this.distanceInMeters = distanceInMeters;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // Reset encoders to drive correct distance
    driveUtil.resetEncoders();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // Drive forward at velocity
    driveUtil.driveStandard(velocity, 0, 0);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    // Stop the robot
    driveUtil.driveStandard(0, 0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    // Distance measured by the back left encoder
    // If traveled distanceInMeters, end; otherwise, continue
    if (driveUtil.getBackLeftEncoder() >= (distanceInMeters * Constants.ticksPerMeter)) {
      return true;
    }
    return false;
  }
}
