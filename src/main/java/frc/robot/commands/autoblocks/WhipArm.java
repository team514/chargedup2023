// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoblocks;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmUtil;

public class WhipArm extends CommandBase {
  ArmUtil armUtil;
  boolean done = false;
  /** Creates a new WhipArm. */
  public WhipArm(ArmUtil armUtil) {
    this.armUtil = armUtil;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(armUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(armUtil.getArmPositionTicks() < 2) {
      armUtil.setArmRotationalSpeed(0.3);
    } else {
      done = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    armUtil.setArmRotationalSpeed(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
