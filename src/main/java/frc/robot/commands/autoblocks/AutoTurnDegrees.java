// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoblocks;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveUtil;

public class AutoTurnDegrees extends CommandBase {
  /** Creates a new TurnAngle. */
  private DriveUtil driveUtil;
  private double targetDegrees;
  private boolean turnRight;
  private boolean done = false;
  public AutoTurnDegrees(DriveUtil driveUtil, double targetDegrees) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
    this.driveUtil = driveUtil;
    this.targetDegrees = targetDegrees;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    driveUtil.resetGyro();
    if(targetDegrees > 0){
      turnRight = true;
    } else {
      turnRight = false;
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(turnRight) {
      if(driveUtil.getGyroAngle() >= targetDegrees){
        driveUtil.driveStandard(0, 0, 0);
        done = true;
        return;
      // } else if(targetDegrees - driveUtil.getRawAngle() <= 20) {
      //   driveUtil.tankDrive(0.2, -0.2);
      } else {
        driveUtil.driveStandard(0, 0, 0.3);
      }
    } else {
      if(driveUtil.getGyroAngle() <= targetDegrees){
        driveUtil.driveStandard(0, 0, 0);
        done = true;
        return;
      // } else if(driveUtil.getRawAngle() - targetDegrees <= 20) {
      //   driveUtil.tankDrive(-0.2, 0.2);
      } else {
        driveUtil.driveStandard(0, 0, -0.3);
      }
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    driveUtil.driveStandard(0, 0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
