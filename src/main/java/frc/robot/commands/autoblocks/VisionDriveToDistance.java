// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoblocks;

import org.photonvision.PhotonUtils;
import org.photonvision.targeting.PhotonTrackedTarget;

import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.VisionUtil;

public class VisionDriveToDistance extends CommandBase {
  private DriveUtil driveUtil;
  private VisionUtil visionUtil;

  /** Creates a new DriveToDistance. */
  public VisionDriveToDistance(DriveUtil driveUtil, VisionUtil visionUtil) {
    this.driveUtil = driveUtil;
    this.visionUtil = visionUtil;

    addRequirements(driveUtil, visionUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    PhotonTrackedTarget target = visionUtil.getBestTarget();
    if (target == null) {
      driveUtil.driveStandard(0, 0, 0);
      return;
    }

    double range = PhotonUtils.calculateDistanceToTargetMeters(0.25, 0.12, Units.degreesToRadians(0),
        Units.degreesToRadians(target.getPitch()));
    System.out.println(range);
    driveUtil.driveStandard(0, 0.7 * range, 0);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
