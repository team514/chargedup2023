// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmUtil;

public class MoveArmPID extends CommandBase {

  ArmUtil armUtil;
  double setpoint;
  boolean done = false;
  boolean wait;

  public MoveArmPID(ArmUtil armUtil, double setpoint) {
    this(armUtil, setpoint, false);
  }

  /** Creates a new MoveArmPID. */
  public MoveArmPID(ArmUtil armUtil, double setpoint, boolean wait) {
    this.armUtil = armUtil;
    this.setpoint = setpoint;
    this.wait = wait;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(armUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    armUtil.setArmSetpoint(setpoint);
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    armUtil.calculatePIDController();
    if(wait) {
      if(armUtil.isAtSetpoint()) {
        done = true;
      }
    } else {
      done = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    armUtil.setArmRotationalSpeed(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
