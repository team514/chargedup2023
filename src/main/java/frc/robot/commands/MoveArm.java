// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmUtil;

public class MoveArm extends CommandBase {
  ArmUtil armUtil;
  boolean up;

  /** Creates a new MoveArm. */
  public MoveArm(ArmUtil armUtil, boolean up) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.armUtil = armUtil;
    this.up = up;
    addRequirements(armUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (up) {
      armUtil.setArmRotationalSpeed(0.1);
    } else {
      armUtil.setArmRotationalSpeed(-0.1);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    armUtil.setArmRotationalSpeed(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
