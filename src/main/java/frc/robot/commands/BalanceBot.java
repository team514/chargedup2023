// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveUtil;

public class BalanceBot extends CommandBase {
  private DriveUtil driveUtil;

  /** Creates a new BalanceBot. */
  public BalanceBot(DriveUtil driveUtil) {
    this.driveUtil = driveUtil;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double pitch = driveUtil.getGyroPitch();
    double speed = (0.8 / 10) * Math.abs(pitch);
    if (pitch > 0) {
      // drive back
      driveUtil.driveStandard(speed, 0, 0); // These speed negations should be flipped, no?
    } else if (pitch < 0) {
      // drive forward
      driveUtil.driveStandard(-speed, 0, 0); // Not sure how accurate pitch is, but maybe if (pitch == 0): drive(0,0,0)?
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
