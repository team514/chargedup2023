// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ExtensionUtil;

public class ExtendArmEncoder extends CommandBase {

  public double extensionTicks; // desired position

  boolean retract; // does the arm need to retract from its initial position
  boolean done = false;

  public ExtensionUtil extensionUtil;
  /** Creates a new ExtendArmEncoder. */
  public ExtendArmEncoder(double extensionTicks, ExtensionUtil extensionUtil) {
    this.extensionTicks = extensionTicks;
    this.extensionUtil = extensionUtil;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(extensionUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if (extensionUtil.getEncoderTicks() < extensionTicks) {
      retract = false;
    } else {
      retract = true;
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override 
  public void execute() {
    if (retract) {
      extensionUtil.setArmExtensionSpeed(-0.5);
      if (extensionUtil.getEncoderTicks() <= extensionTicks) {
        done = true;
      }
    } else {
      extensionUtil.setArmExtensionSpeed(0.5);
      if (extensionUtil.getEncoderTicks() >= extensionTicks) {
        done = true;
      }
    }
  }
    
  // Called once the command ends or is interrupted.
  // Stops all extension movement.
  @Override
  public void end(boolean interrupted) {
    extensionUtil.setArmExtensionSpeed(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
