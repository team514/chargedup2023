// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ExtensionUtil;

public class RetractArmHome extends CommandBase {
  boolean done = false;
  long startTime;

  public ExtensionUtil extensionUtil;
  /** Creates a new RetractArmHome. */
  public RetractArmHome(ExtensionUtil extensionUtil) {
    this.extensionUtil = extensionUtil;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(extensionUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    extensionUtil.setArmExtensionSpeed(-0.5);
    startTime = System.currentTimeMillis();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    extensionUtil.setArmExtensionSpeed(-0.5);

    if (extensionUtil.getEncoderTicks() <= -25) {
      done = true;
    }
    if (System.currentTimeMillis() - startTime >= 3000) {
      done = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    extensionUtil.setArmExtensionSpeed(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
