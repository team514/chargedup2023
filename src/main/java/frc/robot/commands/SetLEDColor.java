// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.handlers.LEDColor;
import frc.robot.subsystems.LEDUtil;

public class SetLEDColor extends CommandBase {
  LEDUtil ledUtil;
  LEDColor ledColor;
  boolean done = false;
  /** Creates a new SetLEDColor. */
  public SetLEDColor(LEDUtil ledUtil, LEDColor ledColor) {
    this.ledUtil = ledUtil;
    this.ledColor = ledColor;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ledUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    ledUtil.setLEDColor(ledColor);
    done = true;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
