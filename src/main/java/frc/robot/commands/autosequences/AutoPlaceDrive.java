// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autosequences;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.ExtendArmEncoder;
import frc.robot.commands.MoveArmPID;
import frc.robot.commands.OperateArm;
import frc.robot.commands.OperateGripper;
import frc.robot.commands.RetractArmHome;
import frc.robot.commands.autoblocks.AutoDriveDistance;
import frc.robot.subsystems.ArmUtil;
import frc.robot.subsystems.CrabUtil;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.ExtensionUtil;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class AutoPlaceDrive extends SequentialCommandGroup {
  DriveUtil driveUtil;
  CrabUtil crabUtil;
  ArmUtil armUtil;
  ExtensionUtil extensionUtil;
  /** Creates a new AutoPlacePiece. */
  public AutoPlaceDrive(DriveUtil driveUtil, CrabUtil crabUtil, ArmUtil armUtil, ExtensionUtil extensionUtil) {
    this.driveUtil = driveUtil;
    this.crabUtil = crabUtil;
    this.armUtil = armUtil;
    this.extensionUtil = extensionUtil;
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new SetupSequence(armUtil, extensionUtil),
      new MoveArmPID(armUtil, Constants.reverseHighSetpoint, true),
      new ParallelRaceGroup(
        new SequentialCommandGroup(
          new ExtendArmEncoder(Constants.reverseHighExtension, extensionUtil),
          // release game piece
          new OperateGripper(crabUtil),
          new WaitCommand(1),
          // retract arm in
          new RetractArmHome(extensionUtil),
          new OperateGripper(crabUtil)
        ),
        new OperateArm(armUtil)
      ),
      new ParallelCommandGroup(
        // lower arm to front while driving away.
        new MoveArmPID(armUtil, 2, true),
        new AutoDriveDistance(driveUtil, 0.6, 3)
      )
    );
  }
}
