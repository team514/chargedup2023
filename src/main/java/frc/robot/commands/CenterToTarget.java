// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import org.photonvision.targeting.PhotonTrackedTarget;

import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.VisionUtil;

public class CenterToTarget extends CommandBase {
  private DriveUtil driveUtil;
  private VisionUtil visionUtil;

  /** Creates a new CenterToTarget. */
  public CenterToTarget(DriveUtil driveUtil, VisionUtil visionUtil) {
    this.driveUtil = driveUtil;
    this.visionUtil = visionUtil;

    addRequirements(driveUtil, visionUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    PhotonTrackedTarget target = visionUtil.getBestTarget();
    if (target == null) {
      driveUtil.driveStandard(0, 0, 0);
      return;
    }

    Transform3d transform = target.getBestCameraToTarget();
    // Negative means it's to the right, positive means it's to the left
    double yOffset = transform.getTranslation().getY();
    if (yOffset <= -0.05) {
      driveUtil.driveStandard(0.4, 0, 0);
    } else if (yOffset >= 0.05) {
      driveUtil.driveStandard(-0.4, 0, 0);
    } else {
      driveUtil.driveStandard(0, 0, 0);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
