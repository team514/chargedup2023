// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.handlers;

import frc.robot.Constants;

/** Add your docs here. */
public enum PathChoice {
    TEST(Constants.testTrajectoryPath);

    String pathName;

    PathChoice(String pathName) {
        this.pathName = pathName;
    }

    public String getPathName() {
        return pathName;
    }
}
