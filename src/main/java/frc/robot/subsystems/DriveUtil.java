// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.controller.HolonomicDriveController;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.MecanumDriveKinematics;
import edu.wpi.first.math.kinematics.MecanumDriveOdometry;
import edu.wpi.first.math.kinematics.MecanumDriveWheelPositions;
import edu.wpi.first.math.kinematics.MecanumDriveWheelSpeeds;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.handlers.MatchState;

public class DriveUtil extends SubsystemBase {
  public static final double kMaxSpeed = 5; // 3 meters per second
  public static final double kMaxAngularSpeed = Math.PI; // 1/2 rotation per second

  // Motor Controllers
  private final WPI_VictorSPX frontLeftMotor = new WPI_VictorSPX(Constants.frontLeft);
  private final WPI_VictorSPX frontRightMotor = new WPI_VictorSPX(Constants.frontRight);
  private final WPI_VictorSPX backLeftMotor = new WPI_VictorSPX(Constants.backLeft);
  private final WPI_VictorSPX backRightMotor = new WPI_VictorSPX(Constants.backRight);

  // Drivebase
  private final MecanumDrive mecanumDrive = new MecanumDrive(frontLeftMotor, backLeftMotor, frontRightMotor, backRightMotor);

  // Encoders
  private final Encoder frontLeftEncoder = new Encoder(Constants.frontLeftEncoder_channelA, Constants.frontLeftEncoder_channelB);
  private final Encoder frontRightEncoder = new Encoder(Constants.frontRightEncoder_channelA, Constants.frontRightEncoder_channelB);
  private final Encoder backLeftEncoder = new Encoder(Constants.backLeftEncoder_channelA, Constants.backLeftEncoder_channelB);
  private final Encoder backRightEncoder = new Encoder(Constants.backRightEncoder_channelA, Constants.backRightEncoder_channelB);

  /*
   * Drive Train PID Controllers
   */

  private final PIDController fieldXPIDController = new PIDController(1, 0, 0);
  private final PIDController fieldYPIDController = new PIDController(1, 0, 0);
  private final ProfiledPIDController fieldRotationPIDController = new ProfiledPIDController(1, 0, 0, new TrapezoidProfile.Constraints(6.28, 3.14));

  private final HolonomicDriveController holonomicDriveController = new HolonomicDriveController(fieldXPIDController, fieldYPIDController, fieldRotationPIDController);

  /*
   * Motor PID Controllers
   */

  // Main PID control
  private final PIDController frontLeftPIDController = new PIDController(1.0, 0, 0);
  private final PIDController frontRightPIDController = new PIDController(1.036, 0, 0);
  private final PIDController backLeftPIDController = new PIDController(1.017, 0, 0);
  private final PIDController backRightPIDController = new PIDController(1.32, 0, 0);

  private final Translation2d frontLeftLocation = new Translation2d(0.428, 0.428);
  private final Translation2d frontRightLocation = new Translation2d(0.428, -0.428);
  private final Translation2d backLeftLocation = new Translation2d(-0.428, 0.428);
  private final Translation2d backRightLocation = new Translation2d(-0.428, -0.428);

  // Feed-forward PID to account for mechanical differences
  // kS = voltage required to just barely move the wheel/overcome static friction
  // kV = volatage required to maintain velocity/overcome kinetic friction
  private final SimpleMotorFeedforward frontLeftFeedforward = new SimpleMotorFeedforward(1.056, 0, 0);
  private final SimpleMotorFeedforward frontRightFeedforward = new SimpleMotorFeedforward(1.357, 0, 0);
  private final SimpleMotorFeedforward backLeftFeedforward = new SimpleMotorFeedforward(1.513, 0, 0);
  private final SimpleMotorFeedforward backRightFeedforward = new SimpleMotorFeedforward(0.727, 0, 0);

  // NavX/Gyro
  private AHRS navX = new AHRS(SPI.Port.kMXP);

  private final MecanumDriveKinematics kinematics = new MecanumDriveKinematics(frontLeftLocation, frontRightLocation, backLeftLocation, backRightLocation);

  private final MecanumDriveOdometry odometry = new MecanumDriveOdometry(kinematics, navX.getRotation2d(), getCurrentDistances());

  // Trajectory
  private Pose2d fieldPose = new Pose2d();
  private Field2d field = new Field2d();

  /** Creates a new DriveUtil. */
  public DriveUtil() {
    frontLeftMotor.setNeutralMode(NeutralMode.Brake);
    frontRightMotor.setNeutralMode(NeutralMode.Brake);
    backLeftMotor.setNeutralMode(NeutralMode.Brake);
    backRightMotor.setNeutralMode(NeutralMode.Brake);

    frontLeftMotor.setInverted(true);
    backLeftMotor.setInverted(true);

    /*
     * We need to calculate how far (in meters) the drive wheels travel every encoder tick.
     * Our encoders have 128 ticks per rotation.
     * We also know our encoders have a 3:1 gear ratio with our drive wheels (encoders rotate 3 times for every 1 drive wheel rotation).
     * Lastly, we know our drive wheels have a circumference of 19.25 inches.
     * That means - 19.25 / 3 = 6.41667 inches traveled per complete encoder rotation.
     * And - 6.41667 / 128 = 0.05013021 inches traveled per encoder tick.
     * Last, we convert 0.05013021 inches to **0.001273307334 meters**.
     */
    frontLeftEncoder.setDistancePerPulse(0.001273307334);
    frontRightEncoder.setDistancePerPulse(0.001273307334);
    backLeftEncoder.setDistancePerPulse(0.001273307334);
    backRightEncoder.setDistancePerPulse(0.001273307334);

    navX.calibrate();
    navX.reset();
  }

  public void teleopInit() {
    Pose2d startingPose = RobotContainer.getCurrentPosition();
    if (startingPose == null) {
      startingPose = new Pose2d(0, 0, new Rotation2d());
    }

    odometry.resetPosition(
        navX.getRotation2d(),
        new MecanumDriveWheelPositions(
            frontLeftEncoder.getDistance(), frontRightEncoder.getDistance(),
            backLeftEncoder.getDistance(), backRightEncoder.getDistance()),
        startingPose);
  }

  /**
   * Returns the current state of the drivetrain.
   *
   * @return The current state of the drivetrain.
   */
  public MecanumDriveWheelSpeeds getCurrentVelocity() {
    return new MecanumDriveWheelSpeeds(
        frontLeftEncoder.getRate(),
        frontRightEncoder.getRate(),
        backLeftEncoder.getRate(),
        backRightEncoder.getRate());
  }

  /**
   * Returns the current distances measured by the drivetrain.
   *
   * @return The current distances measured by the drivetrain.
   */
  public MecanumDriveWheelPositions getCurrentDistances() {
    return new MecanumDriveWheelPositions(
        frontLeftEncoder.getDistance(),
        frontRightEncoder.getDistance(),
        backLeftEncoder.getDistance(),
        backRightEncoder.getDistance());
  }

  /**
   * Set the desired speeds for each wheel.
   *
   * @param speeds The desired wheel speeds.
   */
  public void setMotorSpeeds(MecanumDriveWheelSpeeds speeds) {
    final double frontLeftFeedforward = this.frontLeftFeedforward.calculate(speeds.frontLeftMetersPerSecond);
    final double frontRightFeedforward = this.frontRightFeedforward.calculate(speeds.frontRightMetersPerSecond);
    final double backLeftFeedforward = this.backLeftFeedforward.calculate(speeds.rearLeftMetersPerSecond);
    final double backRightFeedforward = this.backRightFeedforward.calculate(speeds.rearRightMetersPerSecond);

    final double frontLeftOutput =
        frontLeftPIDController.calculate(
            frontLeftEncoder.getRate(), speeds.frontLeftMetersPerSecond);
    final double frontRightOutput =
        frontRightPIDController.calculate(
            frontRightEncoder.getRate(), speeds.frontRightMetersPerSecond);
    final double backLeftOutput =
        backLeftPIDController.calculate(
            backLeftEncoder.getRate(), speeds.rearLeftMetersPerSecond);
    final double backRightOutput =
        backRightPIDController.calculate(
            backRightEncoder.getRate(), speeds.rearRightMetersPerSecond);

    final double frontLeftVoltage = frontLeftOutput + frontLeftFeedforward;
    final double frontRightVoltage = frontRightOutput + frontRightFeedforward;
    final double backLeftVoltage = backLeftOutput + backLeftFeedforward;
    final double backRightVoltage = backRightOutput + backRightFeedforward;

    frontLeftMotor.setVoltage(frontLeftVoltage);
    frontRightMotor.setVoltage(frontRightVoltage);
    backLeftMotor.setVoltage(backLeftVoltage);
    backRightMotor.setVoltage(backRightVoltage);

    // frontLeftMotor.setVoltage(3.0);
    // frontRightMotor.setVoltage(3.0);
    // backLeftMotor.setVoltage(3.0);
    // backRightMotor.setVoltage(3.0);

    SmartDashboard.putNumber("Velocity - FL", speeds.frontLeftMetersPerSecond);
    SmartDashboard.putNumber("Velocity - FR", speeds.frontRightMetersPerSecond);
    SmartDashboard.putNumber("Velocity - BL", speeds.rearLeftMetersPerSecond);
    SmartDashboard.putNumber("Velocity - BR", speeds.rearRightMetersPerSecond);

    SmartDashboard.putNumber("PID Output - FL", frontLeftOutput);
    SmartDashboard.putNumber("PID Output - FR", frontRightOutput);
    SmartDashboard.putNumber("PID Output - BL", backLeftOutput);
    SmartDashboard.putNumber("PID Output - BR", backRightOutput);

    SmartDashboard.putNumber("Feedforward - FL", frontLeftFeedforward);
    SmartDashboard.putNumber("Feedforward - FR", frontRightFeedforward);
    SmartDashboard.putNumber("Feedforward - BL", backLeftFeedforward);
    SmartDashboard.putNumber("Feedforward - BR", backRightFeedforward);

    SmartDashboard.putNumber("Motor Voltage - FL", frontLeftVoltage);
    SmartDashboard.putNumber("Motor Voltage - FR", frontRightVoltage);
    SmartDashboard.putNumber("Motor Voltage - BL", backLeftVoltage);
    SmartDashboard.putNumber("Motor Voltage - BR", backRightVoltage);

    SmartDashboard.putNumber("Encoder Rate - FL", frontLeftEncoder.getRate());
    SmartDashboard.putNumber("Encoder Rate - FR", frontRightEncoder.getRate());
    SmartDashboard.putNumber("Encoder Rate - BL", backLeftEncoder.getRate());
    SmartDashboard.putNumber("Encoder Rate - BR", backRightEncoder.getRate());
  }

  /**
   * Method to drive the robot using joystick info.
   *
   * @param xSpeed Speed of the robot in the x direction (forward).
   * @param ySpeed Speed of the robot in the y direction (sideways).
   * @param rot Angular rate of the robot.
   * @param fieldRelative Whether the provided x and y speeds are relative to the field (aka whether to use gyro)
   */
  public void drivePID(double xSpeed, double ySpeed, double rot, boolean fieldRelative) {
    MecanumDriveWheelSpeeds mecanumDriveWheelSpeeds;
    if (fieldRelative) {
      mecanumDriveWheelSpeeds = kinematics.toWheelSpeeds(
        ChassisSpeeds.fromFieldRelativeSpeeds(xSpeed, ySpeed, rot, navX.getRotation2d()));
    } else {
      mecanumDriveWheelSpeeds = kinematics.toWheelSpeeds(new ChassisSpeeds(xSpeed * kMaxSpeed, ySpeed * kMaxSpeed, rot * Math.PI));
    }
    mecanumDriveWheelSpeeds.desaturate(kMaxSpeed);
    setMotorSpeeds(mecanumDriveWheelSpeeds);
  }

  /**
   * This calls the main driveStandard method but sets squareInputs to false.
   * @param xSpeed is left right movement.
   * @param ySpeed is forwards backwards movement.
   * @param rotation is rotational movement.
   */
  public void driveStandard(double xSpeed, double ySpeed, double rotation) {
    driveStandard(xSpeed, ySpeed, rotation, false);
  }

  /**
   * 
   * @param xSpeed is left right movement.
   * @param ySpeed is forward backward movement.
   * @param rotation is rotational movement.
   * @param squareInputs determines whether we apply a gentler curve.
   */
  public void driveStandard(double xSpeed, double ySpeed, double rotation, boolean squareInputs) {
    if (squareInputs) {
      // Square inputs and carry signs over
      xSpeed = squareInputs(xSpeed);
      ySpeed = squareInputs(ySpeed);
      rotation = squareInputs(rotation);
    }
    mecanumDrive.driveCartesian(xSpeed, ySpeed, rotation);

    SmartDashboard.putNumber("Encoder Value - FL", frontLeftEncoder.getDistance());
    SmartDashboard.putNumber("Encoder Value - FR", frontRightEncoder.getDistance());
    SmartDashboard.putNumber("Encoder Value - BL", backLeftEncoder.getDistance());
    SmartDashboard.putNumber("Encoder Value - BR", backRightEncoder.getDistance());
    
    SmartDashboard.putNumber("Encoder Rate - FL", frontLeftEncoder.getRate());
    SmartDashboard.putNumber("Encoder Rate - FR", frontRightEncoder.getRate());
    SmartDashboard.putNumber("Encoder Rate - BL", backLeftEncoder.getRate());
    SmartDashboard.putNumber("Encoder Rate - BR", backRightEncoder.getRate());
  }

  public double squareInputs(double d) {
    boolean negative = d < 0;
    d = Math.pow(d, 2);
    if (negative) {
      d *= -1;
    }
    return d;
  }

  public double getGyroAngle() {
    return navX.getAngle();
  }

  public void resetGyro() {
    navX.reset();
  }

  public double getGyroPitch() {
    return navX.getPitch();
  }

  public int getFrontLeftEncoder() {
    return frontLeftEncoder.get();
  }

  public int getFrontRightEncoder() {
    return frontRightEncoder.get();
  }

  public int getBackLeftEncoder() {
    return backLeftEncoder.get();
  }

  public int getBackRightEncoder() {
    return backRightEncoder.get();
  }

  public void resetEncoders() {
    frontLeftEncoder.reset();
    frontRightEncoder.reset();
    backLeftEncoder.reset();
    backRightEncoder.reset();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("Encoder - FL", getFrontLeftEncoder());
    SmartDashboard.putNumber("Encoder - FR", getFrontRightEncoder());
    SmartDashboard.putNumber("Encoder - BL", getBackLeftEncoder());
    SmartDashboard.putNumber("Encoder - BR", getBackRightEncoder());
    SmartDashboard.putData("PID - FL", frontLeftPIDController);
    SmartDashboard.putData("PID - FR", frontRightPIDController);
    SmartDashboard.putData("PID - BL", backLeftPIDController);
    SmartDashboard.putData("PID - BR", backRightPIDController);
    SmartDashboard.putNumber("Gyro Value", navX.getAngle());
    SmartDashboard.putNumber("NavX Pitch", navX.getPitch());


    if (RobotContainer.getMatchState().equals(MatchState.AUTO) || RobotContainer.getMatchState().equals(MatchState.TELEOP)) {
      // Get my wheel positions
      var wheelPositions = new MecanumDriveWheelPositions(
          frontLeftEncoder.getDistance(), frontRightEncoder.getDistance(),
          backLeftEncoder.getDistance(), backRightEncoder.getDistance());

      // Get the rotation of the robot from the gyro.
      var gyroAngle = navX.getRotation2d();
      
      // Update the pose
      fieldPose = odometry.update(gyroAngle, wheelPositions);
      field.setRobotPose(fieldPose);

      SmartDashboard.putData("Field", field);
    }

    // var estimator = new MecanumDrivePoseEstimator(kinematics, navX.getRotation2d(), wheelPositions, new Pose2d(),
    //     new MatBuilder<>(Nat.N3(), Nat.N1()).fill(0.02, 0.02, 0.01), // Local measurement standard deviations. Left encoder, right encoder, gyro.
    //     new MatBuilder<>(Nat.N3(), Nat.N1()).fill(0.1, 0.1, 0.01)); // Global measurement standard deviations. X, Y, and theta.
  }
}
