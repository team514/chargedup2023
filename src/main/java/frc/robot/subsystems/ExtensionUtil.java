// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ExtensionUtil extends SubsystemBase {
  private WPI_VictorSPX extensionMotor = new WPI_VictorSPX(Constants.armExtensionMotorController);
  private final Encoder extensionEncoder = new Encoder(Constants.extensionEncoder_channelA, Constants.extensionEncoder_channelB);

  /** Creates a new ExtensionUtil. */
  public ExtensionUtil() {}


  /** Actually moves the arm. */
  public boolean setArmExtensionSpeed(double speed) {
    // If not reached target destination:
    if (speed > 0 && extensionEncoder.get() < Constants.armExtensionMaxTicks){
      extensionMotor.set(speed);
      return true;
    } else if (speed < 0 && extensionEncoder.get() > Constants.armExtensionMinTicks) {
      extensionMotor.set(speed);
      return true;
    } else { // If reached target destination:
      extensionMotor.set(0);
      return false;
    }
  }

  public int getEncoderTicks() {
    return extensionEncoder.get();
  }


  @Override
  public void periodic() {
    SmartDashboard.putNumber("Arm Extension Encoder", extensionEncoder.get());
    // This method will be called once per scheduler run
  }
}
