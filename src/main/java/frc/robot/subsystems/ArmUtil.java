// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ArmUtil extends SubsystemBase {
  private CANSparkMax leftArmMotor = new CANSparkMax(Constants.leftArmMotor, MotorType.kBrushless);
  private CANSparkMax rightArmMotor = new CANSparkMax(Constants.rightArmMotor, MotorType.kBrushless);

  private RelativeEncoder leftArmEncoder = leftArmMotor.getEncoder();
  private RelativeEncoder rightArmEncoder = rightArmMotor.getEncoder();

  // Forward limiter stops the arm from moving into the front of the robot's frame
  // Backward limiter stops the arm from moving into the back of the robot's frame
  // private DigitalInput forwardLimiter = new DigitalInput(Constants.forwardLimiter);
  // private DigitalInput backwardLimiter = new DigitalInput(Constants.backwardLimiter);

 
  private PIDController armPIDController = new PIDController(1, 0, 0);

  /** Creates a new ArmUtil. */
  public ArmUtil() {
    rightArmMotor.setIdleMode(IdleMode.kBrake);
    leftArmMotor.setIdleMode(IdleMode.kBrake);
    rightArmMotor.follow(leftArmMotor, true);
    armPIDController.setTolerance(1);
  }

  public void setArmSetpoint(double encoderTicksSetpoint) {
    armPIDController.setSetpoint(encoderTicksSetpoint);
  }
  
  public double getArmPositionTicks() {
    return leftArmEncoder.getPosition();
  }

  public void calculatePIDController() {
    double voltage = armPIDController.calculate(leftArmEncoder.getPosition());
    if(getArmPositionTicks() < 27) {
      if(voltage < 0) {
        voltage *= 0.2;
        voltage = Math.max(voltage, -1.5);
      } else {
        voltage = Math.min(voltage, 2);
      }
    } else {
      if(voltage > 0) {
        voltage *= 0.2;
        voltage = Math.min(voltage, 1.5);
      } else {
        voltage = Math.max(voltage, -2);
      }
    }
    leftArmMotor.setVoltage(voltage);
  }

  public double getArmSetpoint() {
    return armPIDController.getSetpoint();
  }

  public boolean isAtSetpoint() {
    return armPIDController.atSetpoint();
  }

  /**
   * @param speed If positive, up and away from front. Else, down and towards front.
   * @return Success.
   */
  public boolean setArmRotationalSpeed(double speed) {
    if (speed > 0 && !getBackwardLimiter()) {
      leftArmMotor.set(speed);
      return true;
    } else if (speed < 0 && !getForwardLimiter()) {
      leftArmMotor.set(speed);
      return true;
    } else {
      leftArmMotor.set(0);
      return false;
    }
  }

  /**
   * @param speed If positive, extending. Else, retracting.
   * @return Success.
   */
  

  public boolean getForwardLimiter() {
    // return forwardLimiter.get();
    return false;
  }

  public boolean getBackwardLimiter() {
    // return backwardLimiter.get();
    return false;
  }
  
  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    // SmartDashboard.putNumber("Left Arm Motor Speed", leftArmMotor.get());
    // SmartDashboard.putNumber("Right Arm Motor Speed", rightArmMotor.get());


    SmartDashboard.putNumber("Left Arm Encoder Position", leftArmEncoder.getPosition());
    // SmartDashboard.putNumber("Right Arm Encoder Position", rightArmEncoder.getPosition());

    SmartDashboard.putNumber("Left Arm Encoder Velocity", leftArmEncoder.getVelocity());
    // SmartDashboard.putNumber("Right Arm Encoder Velocity", rightArmEncoder.getVelocity());

    SmartDashboard.putNumber("Arm PID Setpoint", armPIDController.getSetpoint());

    SmartDashboard.putString("ArmUtil Default Command", getCurrentCommand() != null ? getCurrentCommand().getName() : "None");
  }
}
