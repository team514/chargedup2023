// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.handlers.LEDColor;
// import frc.robot.handlers.LEDSegment;

public class LEDUtil extends SubsystemBase {
  AddressableLED ledStrip; // LED Strip
  AddressableLEDBuffer ledBuffer; // Buffer to store color values before writing
  LEDColor globalColor; // Store the current color of LEDs.
  LEDColor topColor, bottomColor, allianceColor;

  int initialHue = 1;

  /** Creates a new LEDUtil. */
  public LEDUtil() {
    /*
     * LEDUtil explanation/purpose
     * This year, there are 4 LED strip segments, two of each on opposing sides of
     * the robot. These segments will be used to show the colors of the balls
     * currently in the robot's magazine instead of the driver and operator having
     * to look towards the dashboard during a match. As far as the code is
     * concerned, there is one strip, as for some reason the code refuses to run
     * with any more.
     */
    ledStrip = new AddressableLED(Constants.ledStripPWM);

    /*
     * Here we set up the lengths of the LEDBuffers. Since there are two segments to
     * each LED strip, the segment length has to be multiplied by the number of
     * segments to get the length of each full strip.
     */
    ledBuffer = new AddressableLEDBuffer(120);

    // Tells LED strip its length
    ledStrip.setLength(ledBuffer.getLength());

    // Start LED strip
    ledStrip.start();

    // Set LEDs to off
    setLEDColor(LEDColor.GREEN);
  }

  // LED INTERACTIONS:
  // Write to LED buffer
  public void writeToBuffer(int bufferOffset, int length, int r, int g, int b) {
    for (var i = bufferOffset; i < length + bufferOffset; i++) {
      /*
       * This for loop iterates from the beginning of the bufferOffset, then while
       * the variable 'i' is less than however long the segment is, plus the
       * bufferOffset (to get the actual position on the strip), the loop runs,
       * and on end, it increases the value of 'i' by one.
       */
      ledBuffer.setRGB(i, r, g, b);
    }
  }

  // Set LED Color
  public void setLEDColor(LEDColor color) {
    // Write LED colors to buffer
    writeToBuffer(0, 120, color.getRed(), color.getGreen(), color.getBlue());

    // Store state values
    this.globalColor = color;

    // Publish the value of the buffer to the LEDs.
    ledStrip.setData(ledBuffer);
  }

  // Rainbow idle mode
  private void rainbow() {
    // For every pixel
    for (var i = 0; i < ledBuffer.getLength(); i++) {
      // Calculate the hue - hue is easier for rainbows because the color
      // shape is a circle so only one value needs to precess
      final var hue = (initialHue + (i * 180 / ledBuffer.getLength())) % 180;
      // Set the value
      ledBuffer.setHSV(i, hue, 255, 128);
    }
    // Increase by to make the rainbow "move"
    initialHue += 4;
    // Check bounds
    initialHue %= 180;

    // Publish the value of the buffer to the LEDs.
    ledStrip.setData(ledBuffer);
  }

  @Override
  public void periodic() {
    /*
     * This method will be called once per scheduler run, updating the LED color
     * each time.
     */
    // setToBallColor();
    // switch (ledMode) {
    //   case RAINBOW:
    //     rainbow();
    //     break;
    //   case BALL:
    //     setToBallColor();
    //     break;
    //   default:
    //     break;
    // }
  }
}
