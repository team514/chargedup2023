// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.io.IOException;
import java.util.Optional;

import org.photonvision.EstimatedRobotPose;
import org.photonvision.PhotonCamera;
import org.photonvision.PhotonPoseEstimator;
import org.photonvision.PhotonPoseEstimator.PoseStrategy;
import org.photonvision.targeting.PhotonTrackedTarget;

import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.handlers.MatchState;
import frc.robot.utils.MathUtil;

public class VisionUtil extends SubsystemBase {
  // Holds field layout file from FIRST
  public static AprilTagFieldLayout FIELD_LAYOUT;

  private PhotonCamera camera;
  private PhotonPoseEstimator photonPoseEstimator;

  // Sets up 2d field where robot position is placed
  private Field2d field = new Field2d();

  /** Creates a new VisionUtil. */
  public VisionUtil() {
    try {
      // Try to find the camera, get the result
      camera = new PhotonCamera("Microsoft_LifeCam_HD-3000");
      camera.getLatestResult();
    } catch (Exception e) {
      // If the camera can't be found, throw an error, don't try to access camera
      System.err.println("[VisionUtil] Error initializing camera!");
      camera = null;
    }
    try {
      // Get this year's field layout in April Tags
      FIELD_LAYOUT = AprilTagFieldLayout.loadFromResource(AprilTagFields.k2023ChargedUp.m_resourceFile);
    } catch (IOException e) {
      // Throw an error if this can't be found
      System.err.println("[VisionUtil] Error initializing field layout!");
    }
    if (camera != null && FIELD_LAYOUT != null) {
      try {
        // Use all AprilTags seen and the field layout to estimate pose
        photonPoseEstimator = new PhotonPoseEstimator(FIELD_LAYOUT, PoseStrategy.MULTI_TAG_PNP, camera, Constants.robotToCamera);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public boolean hasTarget() {
    // Returns true if an AprilTag is seen
    return getBestTarget() != null;
  }

  public PhotonTrackedTarget getBestTarget() {
    if (camera == null) {
      return null;
    }

    var result = camera.getLatestResult();

    // Return nothing if there's no result, or if the result doesn't have a target
    if (result == null || !result.hasTargets()) {
      return null;
    }

    return result.getBestTarget();
  }

  public Optional<EstimatedRobotPose> getPositionOnField() {
    // Returns an estimated position/orientation of robot
    return photonPoseEstimator.update();
  }

  @Override
  public void periodic() {
    // Add MatchState to SmartDashboard
    SmartDashboard.putString("Match State", RobotContainer.getMatchState().name());
    
    // If there's no camera, don't try to use it
    if (camera == null) {
      return;
    }

    if (RobotContainer.getMatchState().equals(MatchState.PREMATCH)) {
      // If still in prematch, keep estimating position
      Optional<EstimatedRobotPose> estimatedPose = getPositionOnField();
      if (estimatedPose.isPresent()) {
        EstimatedRobotPose pose = estimatedPose.get(); // hold estimated pose
        Pose3d pose3d = pose.estimatedPose; // 3D pose
        Pose2d pose2d = pose3d.toPose2d(); // 2D pose
        RobotContainer.setCurrentPosition(pose2d); // Update RobotContainer with current position
        field.setRobotPose(pose2d); // Set the robot on the field object
        // Write coordinates to SmartDashboard and display a field visual
        SmartDashboard.putString("Robot Field Coordinates", "X: " + MathUtil.round(pose2d.getX(), 3) + ", Y: " + MathUtil.round(pose2d.getY(), 3));
        SmartDashboard.putData("Field", field);
      }
    }

    // var result = camera.getLatestResult(); // get results from Raspberry pi
    // // if the result doesn't exist or it exists but there are no targets
    // if (result == null || !result.hasTargets()) {
    //   // put false on the SmartDashboard and stop periodic
    //   SmartDashboard.putBoolean("Has Targets", false);
    //   return;
    // }

    // // List<PhotonTrackedTarget> targets = result.getTargets(); // list of targets
    // PhotonTrackedTarget target = result.getBestTarget(); // find best seen target to track

    // // put information about the target on Smart Dashboard
    // SmartDashboard.putBoolean("Has Targets", result.hasTargets());
    // SmartDashboard.putNumber("Yaw", target.getYaw());
    // SmartDashboard.putNumber("Pitch", target.getPitch());
    // SmartDashboard.putNumber("Area", target.getArea());
    // SmartDashboard.putNumber("Skew", target.getSkew());

    // Transform3d transform = target.getBestCameraToTarget();

    // SmartDashboard.putNumber("Target Translation X", transform.getX());
    // SmartDashboard.putNumber("Target Translation Y", transform.getY());
    // SmartDashboard.putNumber("Target Translation Z", transform.getZ());

    // SmartDashboard.putNumber("Target Rotation X", Math.toDegrees(transform.getRotation().getX()));
    // SmartDashboard.putNumber("Target Rotation Y", Math.toDegrees(transform.getRotation().getY()));
    // SmartDashboard.putNumber("Target Rotation Z", Math.toDegrees(transform.getRotation().getZ()));
  }
}
